# Guess My Number

The computer is trying to guess a number that you have in your head.

## Requirements

* Get the game started: print out the game introduction
* Get number of guesses input from the user
* Store some variables for game state
* Allow the computer to guess num_guesses times
* For each guess, the player indicates too high or too low
* The computer wins if they guess or loses if guesses run out

## Learning Objectives

### Variables and Types

* Recall how to start the Python 3 interpreter from the
  command line using `python`
* Recall how to run a Python file from the command line
  using `python «file_name».py`
* Identify when they're in the Python shell as opposed to
  their terminal shell (Powershell or zsh)
* Recall that the `bool` data type can be one of two
  values, `True` or `False`
* Recall that the `str` data type contains text
* Recall that the `int` data type contains integers
* Recall that the `+` operation works differently for
  `str`s and `int`s
* Recall that the `-` operation works only for `int`s and
  calculates the difference
* Recall that the `*` operation works only for `int`s and
  calculates the product
* Recall that the `/` operation works only for `int`s and
  calculates the quotient
* Recall that the `<` and `>` operators are comparison operations that
  return a `bool` value
* Recall that the `==` operator is a comparison operation that
  returns a `bool` value

### Conditions

* Recall that the `if` statement has a condition to evaluate
  and, if found `True`, will run the Python code that is
  indented beneath it
* Explain the four parts of an `if` statement:
  * The `if` keyword
  * The condition to evaluate
  * The `:` colon line ending
  * The block of code contained by the `if`
* Recall that the `if` statement can have zero or one `else`
  clauses with its own indented code that will evaluate if
  the `if` condition is found to be `False`
* Recall that the `if` statement can have zero or more
  `elif` clauses each with their own indented code that will
  evaluate if the `elif` clause evaluates to `True` and all
  previous `if` or `elif` clauses in the same statement
  evaluated to `False`
* Recall that the empty `str` is considered `False`
* Recall that the `int` 0 is considered `False`

### Loops with ranges

* Explain the six parts of a `for` statement:
  * The `for` keyword
  * The name of the variable to use
  * The `in` keyword
  * The sequence of values
  * The `:` colon line terminator
  * The block of code contained by the `for`
* Explain that the `range` function returns a sequence of
  numbers based on its inputs
* Explain that the `for` loop will, for each value in the
  sequence returned by `range`, set a variable to the value
  and execute the indented block of code with the variable
  set to that value

### Control Flow

* Explain that `if` statements can be nested inside `for`
  statements
* Explain that `for` statements can be nested inside `if`
  statements
* Interpret procedural code containing `if` statements,
  `for-range` statements, variable initialization, variable
  assignment, `print`, and `input` statements

### Lists

* Explain that the form `[...]` is a "list literal" form
  that creates a new list object
* Explain that a list is a value that is an ordered sequence
  of other values
* Explain that a variable can have a list assigned to it
* Explain that they can use the `print` function to see the
  contents of a list
* Explain how to add items to a list using the `append`
  method
* Explain how to get the length of the list by using the
  `len` function
* Explain how to set the value in a list by index
* Explain how to get the value in a list by index

### For Loops with Lists

* Explain that the `for` loop will, for each value in a
  list, set a variable to the value and execute the indented
  block of code with the variable set to that value
* Explain why it is best practice to not change the length
  of a list in a `for` loop because it may result in an
  infinite loop
* Recall that the key combination Ctrl+C will interrupt an
  infinite loop in the Python shell
