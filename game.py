# Get the game started: print out the game introduction
# Get number of guesses input from the user
number = input("Choose a number between 1 and 50 (press enter when ready)\n")

num_guesses = 5

# Store some variables for game state
# This the minimum and maximum numbers they can guess
low = 1
high = 50
guesses = [] # This should hold two things, the guess and the user's answer

# Allow the computer to guess num_guesses times
for current_guess_number in range(num_guesses):  # 0, 1, 2, 3, 4

  # Calculate the average for our guess
  guess = int((low + high) / 2) # Operator Precedence - what order does the code execute?

  # Print the previous guesses like this:
  # Guess: 21, answer: higher
  for guess_answer in guesses:
    print(f"Guess: {guess_answer[0]}, answer: {guess_answer[1]}")

  # Ask the user if this is the right number
  # For each guess, the player indicates too high or too low
  print(f"Is {guess} your number? (correct, higher, lower)")
  answer = input("Answer: ")

  # The computer wins if they guess or loses if guesses run out
  if answer == "correct":
    print("The computer guessed your number!\n")
    print("Hooray!\n")
    exit()
    # The loop won't continue here because we exit()ed the program
  elif answer == "lower":
    # Store the answer and the guess into the guesses list
    guesses.append([guess, answer])
    # if the answer is too high
    # We know the user's number is not 25-50
    high = guess - 1
    # The loop would continue here
  elif answer == "higher":
    guesses.append([guess, answer])
    # if the answer is too low
    # We know the user's number is not 1-25
    low = guess + 1
    # The loop would continue here
  else:
    print("Bad input try again")
    # The loop would continue here

print("\nThe computer didn't guess your number, you win!")